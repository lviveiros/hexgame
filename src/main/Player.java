package main;

import java.awt.Color;
import java.io.Serializable;
import java.util.Random;

/**
 * Defines an abstract Player of the game.
 *
 * @author brenocaraccio
 */
public abstract class Player implements Serializable {

    private Color color;

    private boolean inTurn;

    private PlayerMode mode;

    private int score;

    /*
     * Control variables.
     */
    protected static int PLAYER_COUNT = 0;

    protected Player(Color color, PlayerMode mode) {
        this.color = color;
        this.mode = mode;
    }

    /**
     * Enum of possible Modes of this Player.
     */
    public static enum PlayerMode {

        BOT, HUMAN;
    }

    /**
     * Perform the Winning actions, adding scores to the player.
     */
    public void winRound() {
        this.score += Game.getRoundScore();

        Game.TURN_COUNT = 1;

        // Inverto o turno do vencedor com o perdedor
        Game.PLAYER1.setInTurn(this != Game.PLAYER1);
        Game.PLAYER2.setInTurn(this != Game.PLAYER2);
    }

    /**
     * Resets the Players of the Game.
     */
    public static void resetGame() {
        Game.PLAYER1 = null;
        Game.PLAYER2 = null;

        PLAYER_COUNT = 0;
    }

    /**
     * Returns which of the 2 players are active in Turn.
     *
     * @return The Player that is active in Turn.
     */
    public static Player getInTurnPlayer() {
        if (Game.PLAYER1 == null || Game.PLAYER2 == null) {
            return null;
        }

        return Game.PLAYER1.isInTurn() ? Game.PLAYER1 : Game.PLAYER2;
    }

    /**
     * Gets the Player simple name.
     *
     * @return The Player simple name.
     */
    public String getPlayerName() {
        String botString = "";

        if (this.getMode() == PlayerMode.BOT) {
            botString = " (Máquina)";
        }

        return (this == Game.PLAYER1 ? "Jogador 1" : "Jogador 2") + botString;
    }

    /**
     * Randomize the Turn of the Player 1 and Player 2.
     */
    public static void randomizeTurn() {
        Game.PLAYER1.setInTurn(false);
        Game.PLAYER2.setInTurn(false);

        (new Random().nextBoolean() ? Game.PLAYER1 : Game.PLAYER2).setInTurn(true);
    }

    /**
     * Gets the Player Color.
     *
     * @return Color of this Player.
     */
    public Color getColor() {
        return color;
    }

    /**
     * Sets the Player color.
     *
     * @param color Color of the Player.
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * Checks if this player in is Turn.
     *
     * @return True if the player is in turn or false otherwise.
     */
    public boolean isInTurn() {
        return inTurn;
    }

    /**
     * Sets the inTurn flag for a Player.
     *
     * @param inTurn Flag defining if this player is going to be in Turn.
     */
    public void setInTurn(boolean inTurn) {
        this.inTurn = inTurn;
    }

    /**
     * Gets the Player Mode.
     *
     * @return {@link PlayerMode}.
     */
    public PlayerMode getMode() {
        return mode;
    }

    /**
     * Sets the Player Mode.
     *
     * @param mode {@link PlayerMode}.
     */
    public void setMode(PlayerMode mode) {
        this.mode = mode;
    }

    /**
     * Gets the Player score.
     *
     * @return Score of the player.
     */
    public int getScore() {
        return score;
    }
}
