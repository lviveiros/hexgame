package main;

import java.awt.Color;
import java.util.Random;
import view.hexPiece.HexPiece;
import view.hexPiece.HexPieceActionListener;

/**
 * Defines a Bot Player behavior.
 *
 * @author brenocaraccio
 */
public class BotPlayer extends Player {

    private BotPlayer(Color color) {
        super(color, PlayerMode.BOT);
    }

    /**
     * Perform the A.I action, clicking a Piece defined by a basic search
     * criteria.
     */
    public void perform() {

        // Durmo por 2 segundos, tempo que o bot demora pra pensar!!
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        // TODO: aqui devera ter a logica de I.A.
        HexPiece piece = null;
        while (true) {
            piece = Game.gamePieces.get(new Random().nextInt(Game.gamePieces.size()));

            if (piece.getOwner() == null) {
                break;
            }

            if (piece.getOwner() != null && Game.TURN_COUNT == 2) {
                break;
            }
        }

        // Clico no botão
        new HexPieceActionListener(piece).actionPerformed(null);
    }

    /**
     * Creates a Bot Player.
     *
     * @param color Color related to the Player.
     *
     * @return Brand new BotPlayer instance.
     */
    public static BotPlayer createPlayer(Color color) {
        if (PLAYER_COUNT >= 2) {
            throw new IllegalStateException("Só deveria existir dois jogadores neste jogo!!!");
        }

        PLAYER_COUNT++;

        return new BotPlayer(color);
    }
}
