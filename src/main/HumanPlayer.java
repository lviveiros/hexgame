package main;

import java.awt.Color;

/**
 * Defines a Human Player behavior.
 *
 * @author brenocaraccio
 */
public class HumanPlayer extends Player {

    private HumanPlayer(Color color) {
        super(color, PlayerMode.HUMAN);
    }

    /**
     * Creates a Human Player.
     *
     * @param color Color related to the Player.
     *
     * @return Brand new HumanPlayer instance.
     */
    public static HumanPlayer createPlayer(Color color) {
        if (PLAYER_COUNT >= 2) {
            throw new IllegalStateException("Só deveria existir dois jogadores neste jogo!!!");
        }

        PLAYER_COUNT++;

        return new HumanPlayer(color);
    }
}
