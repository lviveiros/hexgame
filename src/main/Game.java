package main;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import mechanic.BotGameEngine;
import resources.GameConfig;
import utils.GraphUtilities;
import utils.MatrizAdjUtilities;
import static utils.MatrizAdjUtilities.cleanMatrizes;
import utils.SoundUtilities;
import view.hexPiece.HexPiece;
import view.hexPiece.HexPieceActionListener;

/**
 * Represents a Game match, containing the Players, the selected Mode and other
 * Game's information.
 *
 * @author brenocaraccio
 */
public class Game {

    private static GameMode mode;

    public static Player PLAYER1;

    public static Player PLAYER2;

    public static boolean inProgress;

    private static JFrame screen;

    public static int TURN_COUNT;

    public static int ROUND_COUNT;

    private static final int WIN_SCORE = 5;

    private static long elapsedRoundTime;

    private static JPanel board;

    private static JLabel informationLabel;

    private static BotGameEngine engine;

    public static final List<HexPiece> gamePieces = new ArrayList<>();

    /**
     * Starts a Match.
     *
     * @param mode Game Mode.
     * @param boardDimension Board dimension used to create the Pieces
     * dynamically.
     */
    public static void start(GameMode mode, int boardDimension) {
        // se o jogo já não estiver em andamento
        if (inProgress) {
            throw new IllegalStateException("Não deveria estar acontecendo isso!");
        }

        validateScreen();

        Game.mode = mode;
        switch (mode) {
            case HUMANXHUMAN:
                PLAYER1 = HumanPlayer.createPlayer(GameConfig.PLAYER1_DEFAULT_COLOR);
                PLAYER2 = HumanPlayer.createPlayer(GameConfig.PLAYER2_DEFAULT_COLOR);
                break;
            case HUMANXBOT:
                PLAYER1 = HumanPlayer.createPlayer(GameConfig.PLAYER1_DEFAULT_COLOR);
                PLAYER2 = BotPlayer.createPlayer(GameConfig.PLAYER2_DEFAULT_COLOR);
                break;
            case BOTXBOT:
                PLAYER1 = BotPlayer.createPlayer(GameConfig.PLAYER1_DEFAULT_COLOR);
                PLAYER2 = BotPlayer.createPlayer(GameConfig.PLAYER2_DEFAULT_COLOR);
                break;
        }

        GameConfig.BOARD_DIMENSION = boardDimension;

        // Gera o turno aleatório do jogador
        Player.randomizeTurn();

        inProgress = true;
        TURN_COUNT = 1;
        ROUND_COUNT = 1;
        elapsedRoundTime = System.currentTimeMillis();

        createBoard();
        addPanel(Game.board);

        // Sets the Dimension of the Matrix and clean it
        MatrizAdjUtilities.setDimension(boardDimension);
        cleanMatrizes();

        // Inicio a Engine de Bot
        if (mode == GameMode.BOTXBOT || mode == GameMode.HUMANXBOT) {
            engine = new BotGameEngine();
            engine.start();
        }
    }

    /**
     * Resets the game, setting all counters to 0 and starting a new Board.
     */
    public static void resetGame() {
        // Se existe um motor de jogo preciso para-lo
        if (engine != null) {
            engine.die();
            engine = null;
        }

        ROUND_COUNT = 1;
        TURN_COUNT = 1;
        mode = null;
        inProgress = false;

        Player.resetGame();

        screen.setContentPane(new JPanel(null));
        screen.remove(board);

        screen.pack();

        board = null;
        screen.revalidate();
        screen.repaint();
    }

    /**
     * Creates the GameBoard.
     */
    private static void createBoard() {

        JPanel board = new JPanel() {

            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(new ImageIcon(Game.class.getResource("/resources/background.jpg")).getImage(), 0, 0, null);
            }

            @Override
            public Dimension getPreferredSize() {
                return screen.getPreferredSize();
            }

            @Override
            public Dimension getMinimumSize() {
                return getPreferredSize();
            }

            @Override
            public Dimension getMaximumSize() {
                return getPreferredSize();
            }
        };
        board.setLayout(null);
        board.setSize(screen.getPreferredSize());

        informationLabel = new JLabel();
        informationLabel.setBounds(0, 0, 240, 20);
        Font def = informationLabel.getFont();
        informationLabel.setFont(new Font(def.getName(), Font.BOLD, def.getSize() + 8));
        reloadLabelInformations();

        //Adiciono todos os HexPiece numa lista para setar os Vizinhos
//        List<HexPiece> boardPieces = new ArrayList<>();
        board.add(informationLabel);

        //BUG: Peças sendo geradas fora dos limites da tela!
        int H = (screen.getHeight() / GameConfig.BOARD_DIMENSION) / 2;
        int W = (screen.getWidth() / GameConfig.BOARD_DIMENSION) / 2;
        for (int u = 0; u < GameConfig.BOARD_DIMENSION; u++) {
            int numPiece = u;
            for (int i = 0; i < GameConfig.BOARD_DIMENSION; i++) {
                final HexPiece piece = new HexPiece();

                piece.addActionListener(new HexPieceActionListener(piece));
                //150 para 11X11                          
                //10 para 11X11
                //3 para 11x11
                piece.setBounds(screen.getWidth() / 4 + (u * W) - (u * (W / 5)), 20 + (i * H) + (i * 3) + (u * (H / 2)) + (u * 2), W, H);
                piece.setVisible(true);
                piece.setNumPiece(numPiece);
                numPiece += GameConfig.BOARD_DIMENSION;
                board.add(piece);
                gamePieces.add(piece);
//                boardPieces.add(piece);
            }
        }
//        setWhosNextThePiece(boardPieces);

        Game.board = board;

    }

    /**
     * Reloads who's the Player in Turn on the Label Information component.
     */
    private static void reloadLabelInformations() {
        informationLabel.setForeground(Player.getInTurnPlayer().getColor());
        informationLabel.setText(Player.getInTurnPlayer().getPlayerName().toUpperCase());
    }

    /**
     * Destroy and Create an existing Board.
     */
    private static void clearBoard() {
        screen.setContentPane(new JPanel(null));
        screen.remove(board);

        gamePieces.clear();

        createBoard();

        addPanel(board);
        screen.pack();
    }

    /**
     * Set the Game Main Screen.
     *
     * @param screen Main screen to add the Board.
     */
    public static void setMainScreen(JFrame screen) {
        if (Game.screen == screen) {
            return;
        }

        Game.screen = screen;
    }

    /**
     * Validate if the Main Screen is configurated.
     */
    private static void validateScreen() {
        if (screen == null) {
            throw new IllegalStateException("O jogo precisa ter um componente visual principal vinculado via setMainScreen!!");
        }
    }

    /**
     * Adds a JPanel to the Main Screen.
     *
     * @param view Panel a ser adicionado
     */
    private static void addPanel(JPanel view) {
        validateScreen();

        view.setVisible(true);
        screen.setContentPane(view);
        screen.pack();
    }

    /**
     * Change the Player Turn, checking the victory and performing the necessary
     * verifications.
     */
    public static void nextTurn() {

        if (!checkVictory()) {
            // Significa que o jogo foi resetado, só retorno
            if (PLAYER1 == null) {
                return;
            }

            PLAYER1.setInTurn(!PLAYER1.isInTurn());
            PLAYER2.setInTurn(!PLAYER2.isInTurn());

            TURN_COUNT++;
        }

        reloadLabelInformations();
    }

    /**
     * Checks if the Player Won that Round.
     *
     * @return Boolean representing the victory state.
     */
    private static boolean checkVictory() {
        // Vitória
        if (new GraphUtilities().calcula((GameConfig.BOARD_DIMENSION * GameConfig.BOARD_DIMENSION) + 3) >= 1) {

            elapsedRoundTime = System.currentTimeMillis() - elapsedRoundTime;

            Player winner = Player.getInTurnPlayer();

            winner.winRound();

            SoundUtilities.playSound(SoundUtilities.Sound.VICTORY_SOUND);

            JOptionPane.showMessageDialog(screen, "O " + winner.getPlayerName()
                    + " venceu o Round !\n\n"
                    + "Placar\n"
                    + "    Jogador 1: " + PLAYER1.getScore() + " pontos\n"
                    + "    Jogador 2: " + PLAYER2.getScore() + " pontos\n"
                    + "    Tempo de Round: " + (elapsedRoundTime / 1000) + " segundos \n"
                    + " \nFeche a janela para prosseguir ao próximo Round! ",
                    "Fim de Round",
                    JOptionPane.INFORMATION_MESSAGE);

            // Reinicio a contagem de vitória
            elapsedRoundTime = System.currentTimeMillis();

            clearBoard();
            return true;
            // Empate
        } else if (TURN_COUNT == (GameConfig.BOARD_DIMENSION * GameConfig.BOARD_DIMENSION)) {
            elapsedRoundTime = System.currentTimeMillis() - elapsedRoundTime;

            JOptionPane.showMessageDialog(screen, "Empate!\n\n"
                    + "Placar\n"
                    + "    Jogador 1: " + PLAYER1.getScore() + " pontos\n"
                    + "    Jogador 2: " + PLAYER2.getScore() + " pontos\n"
                    + "    Tempo de Round: " + (elapsedRoundTime / 1000) + " segundos \n"
                    + " \nFeche a janela para prosseguir ao próximo Round! ",
                    "Fim de Round",
                    JOptionPane.INFORMATION_MESSAGE);

            // Reinicio a contagem de vitória
            elapsedRoundTime = System.currentTimeMillis();

            TURN_COUNT = 1;

            PLAYER1.setInTurn(!PLAYER1.isInTurn());
            PLAYER2.setInTurn(!PLAYER2.isInTurn());

            clearBoard();
            return true;
        }

        return false;
    }

    /**
     * Calculates the Round Win Score.
     *
     * @return Score of this round.
     */
    public static int getRoundScore() {
        return WIN_SCORE;
    }

    /**
     * Enum representing the possible game Modes.
     */
    public static enum GameMode {

        HUMANXHUMAN, HUMANXBOT, BOTXBOT;
    }

}
