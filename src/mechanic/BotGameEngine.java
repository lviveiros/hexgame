package mechanic;

import main.BotPlayer;
import main.Player;

/**
 * Game Engine to support and perform Bot plays.
 *
 * @author brenocaraccio
 */
public class BotGameEngine {

    private Thread t;

    /**
     * Initialize the Game Bot Engine, which will watch the game every 100 ms.
     */
    public void start() {
        t = new Thread(new Runnable() {

            @Override
            public void run() {

                while (true) {
                    Player inTurnPlayer = Player.getInTurnPlayer();

                    if (inTurnPlayer == null) {
                        return;
                    }

                    if (inTurnPlayer.getMode() == Player.PlayerMode.BOT) {
                        ((BotPlayer) inTurnPlayer).perform();
                    }

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        // se a thread for interrompida, fecho o processamento
                        return;
                    }
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }

    /**
     * Kills the Game Engine.
     */
    public void die() {
        t.interrupt();
    }
}
