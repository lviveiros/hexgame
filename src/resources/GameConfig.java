package resources;

import java.awt.Color;

/**
 * Contains the value of the allowed-to-edit Configurations of the Game.
 *
 * @author brenocaraccio
 */
public class GameConfig {

    public static int BOARD_DIMENSION;

    public static Color PLAYER1_DEFAULT_COLOR = Color.BLUE;

    public static Color PLAYER2_DEFAULT_COLOR = Color.RED;

    public static String VICTORY_SOUND_PATH = GameConfig.class.getResource("/resources/sound_victory.wav").getPath();

    public static String CLICK_SOUND_PATH = GameConfig.class.getResource("/resources/sound_click.wav").getPath();

    public static Color DEFAULT_COLOR = Color.LIGHT_GRAY;
}
