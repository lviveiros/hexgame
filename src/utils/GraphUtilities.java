package utils;

/**
 * Class responsable to calculate the winner. Uses de Djikstra Algorithm to
 * calc.
 *
 * @author leonardo
 */
public class GraphUtilities {

    private static fila inicio = null, fim = null;
    private static int marcado[];
    private static int dist[];
    private static listaAdj adj[];

    private static class vertice {

        private int num;
        private vertice prox;
    }

    private static class listaAdj {

        private vertice vertices;
    }

    private static class fila {

        private int numv;
        private fila prox;
    }

    private static void insert(int n) {
        fila novo = new fila();
        novo.numv = n;
        novo.prox = null;
        if (inicio == null) {
            inicio = fim = novo;
        } else {
            fim.prox = novo;
            fim = novo;
        }
    }

    private static int remove() {
        if (inicio != null) {
            int vert;
            if (inicio == fim) {
                fim = null;
            }
            vert = inicio.numv;
            inicio = inicio.prox;
            return vert;
        }
        return 0;
    }

    private static void widthSearch(listaAdj adj[], int size, int v) {
        vertice listavert;
        int w;
        int vertice;
        marcado[v] = 1;
        dist[v] = 0;
        insert(v);
        while (inicio != null) {
            vertice = remove();
            for (int i = 1; i <= size; i++) {
                listavert = adj[vertice].vertices;
                while (listavert != null) {
                    w = listavert.num;
                    if (marcado[w] == 0) {
                        marcado[w] = 1;
                        dist[w] = dist[vertice] + 1;
                        insert(w);
                    }
                    listavert = listavert.prox;
                }
            }
        }
    }

    /**
     * Shows all Adj vertices. Just used to debug the calculation process.
     *
     * @param adj List of adj vertices
     * @param size Number of pieces
     */
    private static void show_adj(listaAdj adj[], int size) {
        vertice v;
        for (int i = 0; i <= size; i++) {
            v = adj[i].vertices;
            System.out.println("");
            while (v != null) {
                System.out.print("(" + i + "," + v.num + ")" + " ");
                v = v.prox;
            }
        }
    }

    /**
     * Show the distance of each piece from the origin, if exists a Path. Just
     * used to debug the calculation process.
     *
     * @param size Number of pieces
     * @param or Origin piece
     */
    private static void mostrar_dist(int size, int or) {
        System.out.println("Distância da origem " + or + " para o vértice 9\n");
        for (int i = 0; i <= size; i++) {
            System.out.println("" + i + "-" + dist[i]);
        }
    }

    /**
     * Realize the calculation, and find a way, if exists, from origin to
     * destination piece.
     *
     * @param size Number of pieces
     * @param player Matriz of adj
     * @param numPlayer Defines if it's player 1 or 2
     */
    private int calc(int size, int[][] player, int numPlayer) {

        vertice novo;
        //Cria tudo
        adj = new listaAdj[size + 1];
        marcado = new int[size + 1];
        dist = new int[size + 1];

        for (int i = 0; i <= size; i++) {
            adj[i] = new listaAdj();
            marcado[i] = 0;
        }

        for (int i = 0; i < player.length; i++) {
            for (int j = 0; j < player.length; j++) {

                int org = i;
                int dest = j;

                if (player[i][j] == 0) {
                    continue;
                }

                while (org <= size) {

                    //Vizinhança
                    novo = new vertice();
                    novo.num = org;
                    novo.prox = adj[dest].vertices;
                    adj[dest].vertices = novo;
                    break;
                }
            }
        }

        //Busca Largura
        int origemBusca;
        int dista;

        for (int i = 0; i <= size; i++) {
            marcado[i] = 0;
            dist[i] = 0;
        }
        if (numPlayer == 1) {
            origemBusca = player.length - 3;
            widthSearch(adj, size, origemBusca);
//            mostrar_dist(tam, origemBusca);
            dista = dist[player.length - 4];
        } else {
            origemBusca = player.length - 1;
            widthSearch(adj, size, origemBusca);
//            mostrar_dist(tam, origemBusca);
            dista = dist[player.length - 2];
        }

        return dista;
    }

    /**
     * Reset the necessery values to re-use the class. Needed because all
     * variables are static!
     */
    private void clean() {
        inicio = null;
        fim = null;
        for (int n : marcado) {
            marcado[n] = 0;
        }

        for (listaAdj n : adj) {
            n = null;
        }
    }

    /**
     * Find the winner of the game.
     *
     * @param size Number of pieces
     * @return 1 if player one Wins, 2 if Player 2 wins
     */
    public int calcula(int size) {
        int p1 = calc(size, MatrizAdjUtilities.getMatrizPlayer1(), 1);
        clean();
        int p2 = calc(size, MatrizAdjUtilities.getMatrizPlayer2(), 2);
        clean();

        if (p1 == 0 && p2 == 0) {
            return 0;
        }

        if (p1 > p2) {
            MatrizAdjUtilities.setMatrizNeighbours(null);
            return 1;
        }
        MatrizAdjUtilities.setMatrizNeighbours(null);
        return 2;

    }

}
