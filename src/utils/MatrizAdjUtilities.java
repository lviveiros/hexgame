package utils;

import main.Game;
import main.Player;

/**
 * Class responsable to threat all matriz calcs used in game. It contains the
 * pieces of player 1, pieces of player 2 and the neighbours
 *
 * @author leonardo
 */
public class MatrizAdjUtilities {

    private static int matrizNeighbours[][];
    private static int matrizPlayer1[][];
    private static int matrizPlayer2[][];
    private static int dimension = 0;

    public static int getDimension() {
        return dimension;
    }

    public static void setDimension(int aDimension) {
        dimension = aDimension;
    }

    public static int[][] getMatrizNeighbours() {
        return matrizNeighbours;
    }

    public static void setMatrizNeighbours(int[][] aMatrizNeighbours) {
        matrizNeighbours = aMatrizNeighbours;
    }

    public static int[][] getMatrizPlayer1() {
        return matrizPlayer1;
    }

    public static void setMatrizPlayer1(int[][] aMatrizPlayer1) {
        matrizPlayer1 = aMatrizPlayer1;
    }

    public static int[][] getMatrizPlayer2() {
        return matrizPlayer2;
    }

    public static void setMatrizPlayer2(int[][] aMatrizPlayer2) {
        matrizPlayer2 = aMatrizPlayer2;
    }

    public MatrizAdjUtilities() {
        initMatrizNeighbours();
    }

    /**
     * Reset the necessery values to re-use the class. Needed because all
     * variables are static!
     */
    public static void cleanMatrizes() {
//        matrizNeighbours = null;
        initMatrizNeighbours();
//        if (matrizPlayer1 != null) {
//            for (int[] cadaArray : matrizPlayer1) {
//                for (int i = 0; i < matrizPlayer1.length; i++) {
//                    cadaArray[i] = 0;
//                }
//            }
//        }
//        if (matrizPlayer2 != null) {
//            for (int[] cadaArray : matrizPlayer2) {
//                for (int i = 0; i < matrizPlayer2.length; i++) {
//                    cadaArray[i] = 0;
//                }
//            }
//        }
    }

    /**
     * Runs the entirely matriz and set the default neighbours, init the
     * matrizes and .
     */
    private static void initMatrizNeighbours() {
        int pieces = (getDimension() * getDimension()) + 3;

        matrizPlayer1 = new int[pieces + 1][pieces + 1];
        matrizPlayer2 = new int[pieces + 1][pieces + 1];
        matrizNeighbours = new int[pieces][pieces];

        MatrizNeighbourtUtilities matriz = new MatrizNeighbourtUtilities(getDimension());

        matriz.setNeighbour();

        //Pega a matriz de vizinhos criada pelo Hector
        matrizNeighbours = MatrizNeighbourtUtilities.matrix;

        //Seto os matrizNeighbours dos auxiliares
        for (int i = (pieces - 3); i <= pieces; i++) {
            for (int j = 0; j < pieces; j++) {
                matrizPlayer1[i][j] = matrizNeighbours[i][j];
                matrizPlayer2[i][j] = matrizNeighbours[i][j];
            }
        }
    }

    /**
     * Set the neighbours of the piece numPiece. Neighbours are in matriz
     * matrizNeighbours
     *
     * @param t The player that is executing the action
     * @param numPiece The number of the button, used for dijkstra calc
     */
    public static void setNeighbourOfPiece(Player t, int numPiece) {
        if (matrizNeighbours == null) {
            initMatrizNeighbours();
        }
        int totalPieces = (getDimension() * getDimension()) + 3;

        if (t.equals(Game.PLAYER1)) {

            for (int i = 0; i <= totalPieces; i++) {
                //Não seto os matrizNeighbours para os auxiliares do matrizPlayer2            
                if (numPiece != totalPieces
                        && numPiece != totalPieces - 1
                        && i != totalPieces
                        && i != totalPieces - 1) {
                    matrizPlayer1[numPiece][i] = matrizNeighbours[numPiece][i];
                }
            }
        } else {
            for (int i = 0; i <= totalPieces; i++) {
                //Não seto os matrizNeighbours para os auxiliares do matrizPlayer1            
                if (numPiece != totalPieces - 3
                        && numPiece != totalPieces - 2
                        && i != totalPieces - 3
                        && i != totalPieces - 2) {
                    matrizPlayer2[numPiece][i] = matrizNeighbours[numPiece][i];
                }
            }
        }
    }
}
