package utils;

/**
 * Class that will represent the matrix of a graph
 *
 * @author	Hector
 */
public final class MatrizNeighbourtUtilities {

    static int boardSize;
    static int matrix[][];

    /**
     * Build an object MatrizVizinhos that represents the matrix of a graph
     *
     * @param	bS	Size of the board, if it will be 11*11 then the value passed as bS will be 11
     * @author	Hector
     */
    public MatrizNeighbourtUtilities(int bS) {
        setBoardSize(bS);
        setNeighbour();
    }

    /**
     * Prepare the variables boardSize and matriz[][] of the object
     *
     * @param	bS	Size of the board, if it will be 11*11 then the value passed as bS will be 11
     * @author	Hector
     */
    public void setBoardSize(int bS) {
        boardSize = bS;
        matrix = new int[(boardSize * boardSize) + 4][(boardSize * boardSize) + 4];
    }

    /**
     * Prepare all the neighbor pieces of the board
     *
     * @author	Hector
     */
    public void setNeighbour() {
        /*Set extreme vertices*/
        //sup lef
        matrix[0][1] = 1;
        matrix[0][boardSize] = 1;
        matrix[0][boardSize * boardSize] = 1;
        matrix[0][boardSize * boardSize + 2] = 1;

        //sup rig
        matrix[boardSize - 1][boardSize - 2] = 1;
        matrix[boardSize - 1][(boardSize * 2) - 1] = 1;
        matrix[boardSize - 1][(boardSize * 2) - 2] = 1;
        matrix[boardSize - 1][(boardSize * boardSize) + 2] = 1;
        matrix[boardSize - 1][(boardSize * boardSize) + 1] = 1;

        //inf lef
        matrix[boardSize * (boardSize - 1)][boardSize * (boardSize - 2)] = 1;
        matrix[boardSize * (boardSize - 1)][boardSize * (boardSize - 2) + 1] = 1;
        matrix[boardSize * (boardSize - 1)][boardSize * (boardSize - 1) + 1] = 1;
        matrix[boardSize * (boardSize - 1)][(boardSize * boardSize)] = 1;
        matrix[boardSize * (boardSize - 1)][(boardSize * boardSize) + 3] = 1;

        //inf rig
        matrix[(boardSize * boardSize) - 1][(boardSize * boardSize) - 2] = 1;
        matrix[(boardSize * boardSize) - 1][(boardSize * (boardSize - 1)) - 1] = 1;
        matrix[(boardSize * boardSize) - 1][(boardSize * boardSize) + 1] = 1;
        matrix[(boardSize * boardSize) - 1][(boardSize * boardSize) + 3] = 1;
        /*End of set extreme vertices*/

        /*Set outters*/
        for (int i = 0; i <= (boardSize * (boardSize - 1)); i += boardSize) {
            matrix[boardSize * boardSize][i] = 1;
        }

        for (int i = boardSize - 1; i <= (boardSize * boardSize) - 1; i += boardSize) {
            matrix[(boardSize * boardSize) + 1][i] = 1;
        }

        for (int i = 0; i <= boardSize - 1; i++) {
            matrix[(boardSize * boardSize) + 2][i] = 1;
        }

        for (int i = boardSize * (boardSize - 1); i <= (boardSize * boardSize) - 1; i++) {
            matrix[(boardSize * boardSize) + 3][i] = 1;
        }
        /*End of set outters*/

        /*Set extreme lines*/
        //sup
        for (int hex = 1; hex < boardSize - 1; hex++) {
            matrix[hex][hex - 1] = 1;
            matrix[hex][hex + 1] = 1;
            matrix[hex][(hex + boardSize) - 1] = 1;
            matrix[hex][hex + boardSize] = 1;
            matrix[hex][(boardSize * boardSize) + 2] = 1;
        }

        //inf
        for (int hex = (boardSize * boardSize) - (boardSize - 1); hex < (boardSize * boardSize) - 1; hex++) {
            matrix[hex][hex - 1] = 1;
            matrix[hex][hex + 1] = 1;
            matrix[hex][hex - boardSize] = 1;
            matrix[hex][(hex - boardSize) + 1] = 1;
            matrix[hex][(boardSize * boardSize) + 3] = 1;
        }
        /*End of set extreme lines*/

        /*Set extreme columns*/
        //lef
        for (int hex = boardSize; hex < (boardSize * (boardSize - 1)); hex += boardSize) {
            matrix[hex][hex - boardSize] = 1;
            matrix[hex][hex + boardSize] = 1;
            matrix[hex][(hex + 1) - boardSize] = 1;
            matrix[hex][hex + 1] = 1;
            matrix[hex][(boardSize * boardSize)] = 1;
        }

        //rig
        for (int hex = (boardSize * 2) - 1; hex <= (boardSize * (boardSize - 1)) - 1; hex += boardSize) {
            matrix[hex][hex - boardSize] = 1;
            matrix[hex][hex + boardSize] = 1;
            matrix[hex][hex - 1] = 1;
            matrix[hex][(hex - 1) + boardSize] = 1;
            matrix[hex][(boardSize * boardSize) + 1] = 1;
        }
        /*End of set extreme columns*/

        /*Set inner neighbor*/
        for (int hex = boardSize + 1; hex <= boardSize * (boardSize - 1) - 2; hex++) {
            if ((hex + 1) % boardSize == 0) {
                hex += 2;
            }
            matrix[hex][hex - boardSize] = 1;
            matrix[hex][(hex + 1) - boardSize] = 1;
            matrix[hex][hex - 1] = 1;
            matrix[hex][hex + 1] = 1;
            matrix[hex][(hex - 1) + boardSize] = 1;
            matrix[hex][hex + boardSize] = 1;
        }
        /*End of set inner neighbor*/
    }
}
