package utils;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JOptionPane;
import resources.GameConfig;

/**
 * Contains utilitary methods related to the java Sound API.
 *
 * @author brenocaraccio
 */
public class SoundUtilities {

    /**
     * Plays a Sound on a different Thread.
     *
     * @param sound Enum related to the sound to be played.
     */
    public static synchronized void playSound(Sound sound) {
        try {
            File soundFile = new File(sound == Sound.CLICK_SOUND ? GameConfig.CLICK_SOUND_PATH : GameConfig.VICTORY_SOUND_PATH);

            AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);

            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);

            // Reduces the noise
            FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(-20.0f);

            clip.start();

            double soundMs = ((float) (audioIn.getFrameLength() + 0.0)) / audioIn.getFormat().getFrameRate() * 1000;

            //Approach para resolver OutOfMemory Exception causado criar uma thread a cada clip tocado
            //Referência: http://stackoverflow.com/questions/837974/determine-when-to-close-a-sound-playing-thread-in-java
            clip.addLineListener(new LineListener() {
                @Override
                public void update(LineEvent evt) {
                    if (evt.getType() == LineEvent.Type.STOP) {
                        evt.getLine().close();
                    }
                }
            });

        } catch (LineUnavailableException | IOException e) {
            JOptionPane.showMessageDialog(null, "Erro:" + e.getMessage(),
                    "Falha ao tocar música", JOptionPane.ERROR_MESSAGE);
        } catch (UnsupportedAudioFileException ex) {
            JOptionPane.showMessageDialog(null, "Esta aplicação só aceita áudios no formato .WAV",
                    "Formato de Áudio inválido", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Define the possibles Sounds on the system.
     */
    public static enum Sound {

        CLICK_SOUND, VICTORY_SOUND;
    }
}
