package view.hexPiece;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import main.Game;
import static main.Game.PLAYER1;
import static main.Game.PLAYER2;
import main.Player;
import resources.GameConfig;
import utils.MatrizAdjUtilities;
import utils.SoundUtilities;

/**
 * Defines the sequence of Actions when a HexPiece is clicked.
 *
 * @author brenocaraccio
 */
public class HexPieceActionListener implements ActionListener {

    private final HexPiece relatedPiece;

    public HexPieceActionListener(HexPiece relatedPiece) {
        this.relatedPiece = relatedPiece;
    }

    /**
     * Add actionListener that plot who is the neighbours of each piece. Used to
     * calculate the winner, based on Dijkstra's Algorithm
     *
     * @param e Event performed on piece
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        // Se existir um evento por trás, significa que chegou aqui por conta de um clique de usuário
        // então se for a vez do Bot, e o usuário clicar, simplesmente ignoro a ação do usuário
        if (e != null
                && Player.getInTurnPlayer().getMode() == Player.PlayerMode.BOT) {
            return;
        }

        if ((relatedPiece.isHexagonSelected() && Game.TURN_COUNT == 2)
                || !relatedPiece.isHexagonSelected()) {

            Player inTurnPlayer = Player.getInTurnPlayer();
            if (inTurnPlayer == null) {
                return;
            }

            relatedPiece.setOwner(inTurnPlayer);
            relatedPiece.setHexagonSelected(true);

            relatedPiece.revalidate();
            relatedPiece.repaint();

            SoundUtilities.playSound(SoundUtilities.Sound.CLICK_SOUND);

            MatrizAdjUtilities.setDimension(GameConfig.BOARD_DIMENSION);
            if (Player.getInTurnPlayer().equals(PLAYER1)) {
                MatrizAdjUtilities.setNeighbourOfPiece(PLAYER1, relatedPiece.getNumPiece());
            } else if (Player.getInTurnPlayer().equals(PLAYER2)) {
                MatrizAdjUtilities.setNeighbourOfPiece(PLAYER2, relatedPiece.getNumPiece());
            }

            Game.nextTurn();
        }
    }
}
