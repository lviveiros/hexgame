package view.hexPiece;

import main.Player;
import java.awt.Color;
import java.awt.Graphics;
import resources.GameConfig;

/**
 * Class related to a Piece of the HexGame. <br\>
 * Contains particularity to the Hexagonal Button component, including new
 * fields and click-behaviors.
 *
 * @author brenocaraccio
 */
public class HexPiece extends HexagonalButton {

    private boolean hexagonSelected = false;

    private Player owner;

    private int numPiece = 0;

    /**
     * {@inheritDoc}.
     *
     * @param g Graphics.
     */
    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        g.fillRect(0, 0, getWidth(), getHeight());

        if (hexagonSelected) {
            g.setColor(owner.getColor());
        } else {
            g.setColor(GameConfig.DEFAULT_COLOR);
        }
        g.drawPolygon(hexagonalShape);
        g.fillPolygon(hexagonalShape);
    }

    /**
     * Returns if the Hexagonal part is clicked or not.
     *
     * @return Boolean representing the state.
     */
    public boolean isHexagonSelected() {
        return hexagonSelected;
    }

    /**
     * Sets the state of the Hexagonal selection.
     *
     * @param hexagonSelected Boolean representing the selection state.
     */
    public void setHexagonSelected(boolean hexagonSelected) {
        this.hexagonSelected = hexagonSelected;
    }

    /**
     * Returns who's the 'owner' of this Piece.
     *
     * @return Player who owns the piece.
     */
    public Player getOwner() {
        return owner;
    }

    /**
     * Sets the owner of this button.
     *
     * @param owner Player to be the owner.
     */
    public void setOwner(Player owner) {
        this.owner = owner;
    }

    /**
     * Gets the referent number of this piece on the board.
     *
     * @return The int number of this piece.
     */
    public int getNumPiece() {
        return numPiece;
    }

    /**
     * Sets the number of this piece related to the board.
     *
     * @param numPiece Number of the piece.
     */
    public void setNumPiece(int numPiece) {
        this.numPiece = numPiece;
    }
}
