package view.hexPiece;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.JButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;

/**
 * Basic Hexagonal button component.
 */
public class HexagonalButton extends JButton implements Serializable {

    protected Polygon hexagonalShape;

    public HexagonalButton() {
        this.setOpaque(false);
        hexagonalShape = getHexPolygon();
    }

    /**
     * Returns an Polygon in the Hexagonal shape.
     *
     * @return New polygon.
     */
    private Polygon getHexPolygon() {
        Polygon hex = new Polygon();
        int w = getWidth() - 1;
        int h = getHeight() - 1;
        int ratio = (int) (w * .25);

        hex.addPoint(ratio, 0);
        hex.addPoint(w - ratio, 0);
        hex.addPoint(w, h / 2);
        hex.addPoint(w - ratio, h);
        hex.addPoint(ratio, h);
        hex.addPoint(0, h / 2);

        return hex;
    }

    /*
     * (non-Javadoc).
     */
    @Override
    public boolean contains(Point p) {
        return hexagonalShape.contains(p);
    }

    /*
     * (non-Javadoc).
     */
    @Override
    public boolean contains(int x, int y) {
        return hexagonalShape.contains(x, y);
    }

    /*
     * (non-Javadoc).
     */
    @Override
    public void setSize(Dimension d) {
        super.setSize(d);
        hexagonalShape = getHexPolygon();
    }

    /*
     * (non-Javadoc).
     */
    @Override
    public void setSize(int w, int h) {
        super.setSize(w, h);
        hexagonalShape = getHexPolygon();
    }

    /*
     * (non-Javadoc).
     */
    @Override
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
        hexagonalShape = getHexPolygon();
    }

    /*
     * (non-Javadoc).
     */
    @Override
    public void setBounds(Rectangle r) {
        super.setBounds(r);
        hexagonalShape = getHexPolygon();
    }

    /*
     * (non-Javadoc).
     */
    @Override
    protected void processMouseEvent(MouseEvent e) {
        if (contains(e.getPoint())) {
            super.processMouseEvent(e);
        }
    }

    /*
     * (non-Javadoc).
     */
    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.GRAY);
        g.drawPolygon(hexagonalShape);
        g.fillPolygon(hexagonalShape);
    }

    /**
     * @see javax.swing.AbstractButton#paintBorder(java.awt.Graphics)
     */
    @Override
    protected void paintBorder(Graphics g) {
        // Nao printa a borda
    }

    public Polygon getHexagonalShape() {
        return hexagonalShape;
    }

    public void setHexagonalShape(Polygon hexagonalShape) {
        this.hexagonalShape = hexagonalShape;
    }

    public ChangeListener getChangeListener() {
        return changeListener;
    }

    public void setChangeListener(ChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    public ActionListener getActionListener() {
        return actionListener;
    }

    public void setActionListener(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public ItemListener getItemListener() {
        return itemListener;
    }

    public void setItemListener(ItemListener itemListener) {
        this.itemListener = itemListener;
    }

    public ChangeEvent getChangeEvent() {
        return changeEvent;
    }

    public void setChangeEvent(ChangeEvent changeEvent) {
        this.changeEvent = changeEvent;
    }

    public ComponentUI getUi() {
        return ui;
    }

    public void setUi(ComponentUI ui) {
        this.ui = ui;
    }

    public EventListenerList getListenerList() {
        return listenerList;
    }

    public void setListenerList(EventListenerList listenerList) {
        this.listenerList = listenerList;
    }
}
