package view.sobre;

/**
 * Help Screen definitions.
 */
public class Help extends javax.swing.JDialog {

    private static final String HELP_TEXT = ""
            + "<html>"
            + "HexGame  é um jogo de tabuleiro cuja decisão do Vencedor gira em torno de qual "
            + "dos Jogadores formam um caminho de peças de sua Cor definida de uma extremidade até outra extremidade."
            + "</html>";

    public Help(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        labelAbout.setText(HELP_TEXT);
        setLocationRelativeTo(parent);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        labelAbout = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ajuda");
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Ajuda sobre o jogo");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(10, 10, 150, 14);

        labelAbout.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        getContentPane().add(labelAbout);
        labelAbout.setBounds(20, 30, 250, 100);

        setBounds(0, 0, 288, 174);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel labelAbout;
    // End of variables declaration//GEN-END:variables
}
