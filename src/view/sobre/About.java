package view.sobre;

import java.awt.Frame;

/**
 * About Dialog of the project.
 *
 * @author leonardo
 */
public class About extends javax.swing.JDialog {

    /**
     * Constant referencing the text to be inserted in the "About".
     */
    private static final String[] AUTHORS = new String[]{
        "Breno S. Caraccio - 154816",
        "Leonardo V. &#8594 156233",
        "Hector G. Encinas - 155693"};
//            + "<html> "
//            + "  <tr> "
//            + "    <td> Breno S. Caraccio - 154816 </td>"
//            + "  </tr> "
//            + "  <tr> "
//            + "    <td> Leonardo V. &#8594 156233 </td>"
//            + "  </tr> "
//            + "  <tr> "
//            + "    <td> Hector G. Encinas - 155693 </td>"
//            + "  </tr> "
//            + "  <tr> "
//            + "    <td> Alguem </td>"
//            + "  </tr> "
//            + "</html> ";

    /**
     * Creates new form Sobre
     *
     * @param parent
     * @param modal
     */
    public About(Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(parent);

        labelSobre.setText(getAbout());
        setResizable(false);
    }

    /**
     * Returns the About default text.
     *
     * @return About text formatted in HTML.
     */
    private String getAbout() {
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        for (String author : AUTHORS) {
            sb.append("<tr>");
            sb.append("<td>");
            sb.append(author);
            sb.append("</td>");
            sb.append("</tr>");

        }
        sb.append("</html>");

        return sb.toString();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelSobre = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Desenvolvedores");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        labelSobre.setText("labelSobre");
        labelSobre.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        labelSobre.setBorder(javax.swing.BorderFactory.createTitledBorder("Sobre\n"));
        labelSobre.setPreferredSize(new java.awt.Dimension(320, 150));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelSobre, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelSobre, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel labelSobre;
    // End of variables declaration//GEN-END:variables
}
