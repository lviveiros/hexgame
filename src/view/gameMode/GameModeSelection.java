package view.gameMode;

import main.Game;

/**
 * Interface that defines the behavior of a Game Mode selection.
 *
 * @author brenocaraccio
 */
public interface GameModeSelection {

    /**
     * Select an Game Mode.
     *
     * @return Return the Game Mode selected.
     */
    public Game.GameMode selectMode();

    /**
     * Gets the specified Board Dimension.
     *
     * @return int representing the board dimension (12 = 12x12, etc).
     */
    public int getBoardDimension();
}
