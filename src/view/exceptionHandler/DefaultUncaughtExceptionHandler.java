package view.exceptionHandler;

import java.awt.Frame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

/**
 * Class responsible to render an Panel with uncaugth exception, and show it to
 * user in a Dialog
 *
 * @author leonardo
 */
public class DefaultUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    /**
     * {@inheritDoc}
     */
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        showError(t, e);
    }

    /**
     * Show Panel with any uncaught Exception.
     *
     * @param t actual thread
     * @param e throwable exception
     */
    private static void showError(Thread t, Throwable e) {
        JPanel exceptionPanel = new JPanel();
        JTextArea textException = new JTextArea(15, 50);

        JScrollPane scroll = new JScrollPane(textException);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        StringBuilder sb = new StringBuilder();

        sb.append(e.toString())
                .append("\n");
        sb.append(e.getMessage() != null ? e.getMessage() : "");

        //Se nao foi RuntimeException, tem cause
        if (e.getCause() != null) {
            sb.append("\n");
            sb.append(e.getCause().toString());
            sb.append("\n");
        }

        for (StackTraceElement stack : e.getStackTrace()) {
            sb.append(stack.toString());
            sb.append("\n");
        }

        textException.setText(sb.toString());
        textException.setCaretPosition(0);

        exceptionPanel.add(scroll);
        exceptionPanel.setVisible(true);
        JOptionPane.showMessageDialog(getActiveFrame(), exceptionPanel,
                "Erro Inesperado", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Return the currently active frame. <br/>Reference:
     * http://www.javapractices.com/topic/TopicAction.do?Id=230
     *
     * @return active frame
     */
    public static Frame getActiveFrame() {
        Frame result = null;
        Frame[] frames = Frame.getFrames();
        for (Frame frame : frames) {
            if (frame.isVisible()) {
                result = frame;
                break;
            }
        }
        return result;
    }

}
